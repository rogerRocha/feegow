# Apresentação

  Projeto simples baseado nos teste solicitado pela Feegow, utilizado framework laravel


# Levantando ambiente

Tendo o docker e docker-compose instalados na máquina, basta executar o comando docker-compose up, certifique-se de que as portas 9999 e 3306 na sua máquina não estejam sendo utilizadas.

Siga os seguintes passos:

Execute a instrução abaixo para instalar as dependência do php
docker run --rm -it -v $(pwd):/app -w /app/projeto composer install

Levantar os containers do Docker atraves do comando docker-compose up

Abra um novo terminal e utilize o comando abaixo para criar as tabelas necessárias.
docker exec -it feegow-php php artisan migrate
No navegador, acesse http://localhost:9999/.

Caso a porta esteja não consiga utilizar na porta informada, entre na pasta projeto e utilize o seguinte comando
docker exec -it feegow-php php artisan serve --host 0.0.0.0  --port 9999


