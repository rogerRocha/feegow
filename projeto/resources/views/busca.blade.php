<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .row.heading h2 {
        color: #fff;
        font-size: 52.52px;
        line-height: 95px;
        font-weight: 400;
        text-align: center;
        margin: 0 0 40px;
        padding-bottom: 20px;
        text-transform: uppercase;
    }

    ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .padding-lg {
        display: block;
        padding-top: 30px;
        padding-bottom: 60px;
    }


    .our-webcoderskull .cnt-block:hover {
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.3);
        border: 0;
    }

    .practice-area .inner h3 {
        color: #3c3c3c;
        font-size: 24px;
        font-weight: 500;
        font-family: 'Poppins', sans-serif;
        padding: 10px 0;
    }

    .practice-area .inner p {
        font-size: 14px;
        line-height: 22px;
        font-weight: 400;
    }

    .practice-area .inner img {
        display: inline-block;
    }

    section {
        height: 100%;
    }

    .our-webcoderskull {
        background: rgba(0, 0, 0, .03);

    }

    .our-webcoderskull .cnt-block {
        float: left;
        width: 100%;
        background: #fff;
        padding: 30px 20px;
        text-align: center;
        border: 2px solid #d5d5d5;
        margin: 0 0 28px;
    }

    .our-webcoderskull .cnt-block figure {
        width: 148px;
        height: 148px;
        border-radius: 100%;
        display: inline-block;
        margin-bottom: 15px;
    }

    .our-webcoderskull .cnt-block img {
        width: 148px;
        height: 148px;
        border-radius: 100%;
    }

    .our-webcoderskull .cnt-block h3 {
        color: #2a2a2a;
        font-size: 20px;
        font-weight: 500;
        padding: 6px 0;
        text-transform: uppercase;
    }

    .our-webcoderskull .cnt-block h3 a {
        text-decoration: none;
        color: #2a2a2a;
    }

    .our-webcoderskull .cnt-block h3 a:hover {
        color: #337ab7;
    }

    .our-webcoderskull .cnt-block p {
        color: #2a2a2a;
        font-size: 13px;
        line-height: 20px;
        font-weight: 400;
    }

    .our-webcoderskull .cnt-block .follow-us {
        margin: 20px 0 0;
    }

    .our-webcoderskull .cnt-block .follow-us li {
        display: inline-block;
        width: auto;
        margin: 0 5px;
    }

    .margin-down {
        margin-bottom: 2rem !important;
    }


</style>
<body>
<section class="our-webcoderskull padding-lg">
    <div class="container">
        @if (isset($especialidades))
            <div class="container">
                <div class="card mt-5 margin-down">
                    <div class="card-header">Busca por especialidade</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('pesquisar')}}">
                            @csrf
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label>Quero me consultar com um</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select id="especialidade" name="especialidade" required="required"
                                                class="form-control form-control-sm">
                                        <option value="-1">Escolha uma especialidade</option>
                                        @foreach($especialidades as $especialidade)
                                                <option value="{{$especialidade->especialidade_id}}">{{$especialidade->nome}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="submit" class="btn btn-info btn-sm">
                                            <i class="fas fa-search"></i> Buscar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
        @if (isset($medicos))
            <ul class="row">
                @foreach($medicos as $medico)
                    <li class="col-12 col-md-6 col-lg-3">
                        <div class="cnt-block equal-hight" style="height: 349px;">
                            @if ($medico->foto)
                                <figure><img src="{{$medico->foto}}" class="img-responsive" alt=""></figure>
                            @else
                                <figure><img src="https://image.flaticon.com/icons/png/512/64/64096.png"
                                             class="img-responsive" alt=""></figure>
                            @endif
                            <h5>{{$medico->nome}}</h5>
                            <ul class="follow-us clearfix">
                                @if ($medico->conselho && $medico->documento_conselho)
                                    <li>{{$medico->conselho}} - {{$medico->documento_conselho}}</li>
                                @else
                                    <li>Documento não cadastrado.</li>
                                @endif
                            </ul>
                            <ul class="follow-us clearfix">
                                <li>
                                    <form method="POST" action="{{ route('formAgendamento') }}">
                                        @csrf
                                        <input type="hidden" value="{{$medico->especialidades[0]->especialidade_id}}" name="specialty_id" />
                                        <input type="hidden" value="{{$medico->especialidades[0]->nome_especialidade}}" name="nome_especialidade" />
                                        <input type="hidden" value="{{$medico->profissional_id}}" name="professional_id" />
                                        <input type="hidden" value="{{$medico->nome}}" name="nome_profissional" />
                                        <div class="text-center padding-10">
                                            <button type="submit" class="btn btn-info btn-sm">
                                                <i class="fas fa-calendar-alt"></i> Agendar
                                            </button>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</section>
</body>