<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
    body {
        background: rgba(0, 0, 0, .03);
    }

    margin-down {
        margin-bottom: 2px;
    }
</style>

<body>
<div class="container">
    <div class="modal" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Consulta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>A consulta foi cadastrada com sucesso, para realizar uma nova consulta, clique no botão abaixo.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="redirect()">Nova consulta</button>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
<script>
function redirect() {
    window.location = '/';
}
$( document ).ready(function() {
    $("#myModal").show();
});
</script>
</html>
