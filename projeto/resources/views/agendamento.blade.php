<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
    body {
        background: rgba(0, 0, 0, .03);
    }

    margin-down {
        margin-bottom: 2px;
    }
</style>

<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header">Agendar</div>
        <div class="card-body">
            <form method="POST" action="{{ route('agendar') }}">
                @csrf
                <p>Agendar consulta com <b>{{$escolha->nome_profissional}}</b>, na especialidade de <b>{{$escolha->nome_especialidade}}</b>
                    .</p>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-2 margin-down">
                            <label for="name">Nome completo</label>
                        </div>
                        <div class="col-md-4 margin-down">
                            <input type="text" id="name" name="name" required="required" autofocus="autofocus"
                                   class="form-control form-control-sm">
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-2">
                            <label for="source_id">Como conheceu?</label>
                        </div>
                        <div class="col-md-3">
                            <select id="source_id" name="source_id" required="required" autofocus="autofocus"
                                    class="form-control form-control-sm">
                                @foreach($origens as $origem)
                                    <option value="{{$origem->origem_id}}">{{$origem->nome_origem}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    </br>
                    <div class="form-row">
                        <div class="col-md-2">
                            <label for="birthdate">Data de nascimento</label>
                        </div>
                        <div class="col-md-4">
                            <input type="date" id="birthdate" name="birthdate" required="required" autofocus="autofocus"
                                   class="form-control form-control-sm">
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-2">
                            <label for="cpf">CPF</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" id="cpf" name="cpf"
                                   title="Apenas numeros, com apenas 11 digitos"
                                   pattern="\d{11}" required="required" autofocus="autofocus"
                                   class="form-control form-control-sm">
                        </div>
                    </div>
                    </br>

                    <input type="hidden" id="specialty_id" name="specialty_id"
                           value="{{$escolha->specialty_id}}"/>
                    <input type="hidden" id="professional_id" name="professional_id"
                           value="{{$escolha->professional_id}}"/>
                    <div class="text-center padding-10">
                        <button type="submit" class="btn btn-info btn-sm">
                            <i class="fas fa-save"></i> Confirmar
                        </button>
                        <a href="{{ route('pesquisar') }}">
                            <button class="btn btn-danger btn-sm">cancelar</button>
                        </a>
                    </div>

                </div>
            </form>
        </div>
    </div>

</div>
</body>
</html>
