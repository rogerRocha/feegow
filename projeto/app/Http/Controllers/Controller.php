<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $api = [
        'base_uri' => 'http://clinic5.feegow.com.br/components/public/api/',
        'headers' => ['x-access-token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJmZWVnb3ciLCJhdWQiOiJwdWJsaWNhcGkiLCJpYXQiOiIxNy0wOC0yMDE4IiwibGljZW5zZUlEIjoiMTA1In0.UnUQPWYchqzASfDpVUVyQY0BBW50tSQQfVilVuvFG38']
    ];

    public function getApi($rota, $par = null)
    {
        $client = new Client($this->api);
        if (isset($par))
            $response = $client->get($rota, $par);
        else
            $response = $client->get($rota);
        $resultado = json_decode($response->getBody());
        return $resultado->content;
    }
}
