<?php

namespace App\Http\Controllers;

use App\Agendamento;
use Illuminate\Http\Request;

class AgendamentoController extends Controller
{

    public function index()
    {
        $especialidades = $this->getApi('specialties/list');
        return view('busca', compact('especialidades'));
    }

    public function pesquisar(Request $request)
    {
        $dados = $request->all();
        $especialidades = $this->getApi('specialties/list');
        $medicos = $this->getApi('professional/list', ['json' => ['especialidade_id' => $dados['especialidade']]]);
        return view('busca', compact('especialidades', 'medicos'));
    }

    public function formAgendamento(Request $escolha)
    {
        $origens = $this->getApi('patient/list-sources');
        return view('agendamento', compact('escolha', 'origens'));
    }

    public function agendar(Request $request)
    {
        $data = $request->all();
        $data['date_time'] = date("Y-m-d H:i:s");
        unset($data['_token']);
        Agendamento::create($data);
        return view('modal');
    }

}
