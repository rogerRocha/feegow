<?php

Route::get('/', 'AgendamentoController@index');
Route::get('/pesquisar', function(){
    return redirect('/');
})->name('pesquisar');
Route::get('/agendamento', function(){
    return redirect('/');
})->name('formAgendamento');

Route::post('/pesquisar', 'AgendamentoController@pesquisar')->name('pesquisar');
Route::post('/agendamento', 'AgendamentoController@formAgendamento')->name('formAgendamento');
Route::post('/agendar', 'AgendamentoController@agendar')->name('agendar');

